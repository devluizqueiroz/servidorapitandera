from requests import get

class infoIP:
        
    def __init__(self, ip, key):
        self.key = key
        self.ip = ip
        self.getIPGeo()

    def getIPGeo(self):

        information = get(
            'http://api.ipstack.com/{}?access_key={}'.format(self.ip, self.key))
        information = information.json()
        informationJson = {
        "cidade": information['city'],
        "pais": information['country_name'],
        "estado": information['region_name'],
        "lati": information['latitude'],
        "long": information['longitude'],
        #"as":information['as']
        }
        return informationJson
