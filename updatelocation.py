import peewee
from iptools import apigeoip

db = peewee.SqliteDatabase('003.db')


class Camera(peewee.Model):
    ip = peewee.CharField()
    ip_host = peewee.CharField()
    asn = peewee.CharField()
    cidade = peewee.CharField()
    pais = peewee.CharField()
    estado = peewee.CharField()
    lati = peewee.CharField()
    longt = peewee.CharField()
    title = peewee.CharField()
    data_add = peewee.DateTimeField()
    bot2 = peewee.BooleanField()

    class Meta:
        database =db

key="9f1dae73fe418a12764f31302b239236"



for x in Camera.select():
    print(x)
    if x.bot2 == 0:
        resp = apigeoip.infoIP(x.ip, key).getIPGeo()
        x.cidade=resp["cidade"]
        x.pais=resp["pais"]
        x.lati=resp["lati"]
        x.longt=resp["long"]
        x.estado=resp["estado"]
        x.bot2=1
        x.save()
        print(x.cidade)
        print(x.ip)
    else:
        pass