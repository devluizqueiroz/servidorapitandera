import peewee

db = peewee.SqliteDatabase('003.db')


class Camera(peewee.Model):
    ip = peewee.CharField()
    ip_host = peewee.CharField()
    asn = peewee.CharField()
    cidade = peewee.CharField()
    pais = peewee.CharField()
    estado = peewee.CharField()
    lati = peewee.CharField()
    longt = peewee.CharField()
    title = peewee.CharField()
    data_add = peewee.DateTimeField()
    bot2 = peewee.BooleanField()

    class Meta:
        database =db

#
# db.connect()
# Camera.create_table()
