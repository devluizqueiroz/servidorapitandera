from Servidor.Models import ModelsIP


class ManagerIpsCollector:

    def __init__(self):
        self.ModelIP = ModelsIP.Camera

    def getIpApi(self):
        Result = []
        for item in self.ModelIP.select():

            Result.append(
                {
                    "id": item.id,
                    "ip": item.ip,
                    "ip_host": item.ip_host,
                    "asn": item.asn,
                    "cidade": item.cidade,
                    "estado": item.estado,
                    "title": item.title,
                    "data_add": item.data_add
                }
            )

        return Result


    def postSaveApi(self, Json):
        self.ModelIP.create(
            ip=Json['ip'],
            ip_host=Json['ip_host'],
            asn=Json['as'],
            cidade=Json['cidade'],
            pais=Json['pais'],
            estado=Json['estado'],
            lati=Json['lati'],
            longt=Json['longt'],
            title=Json['title'],
            data_add=Json['data_add'],
            bot2=False
        ).save()
        return True