from flask import Flask, render_template, request
from Servidor.Models.FunctionsConsult import ManagerIpsCollector
import os

app = Flask(__name__)


dbFunc = ManagerIpsCollector()


@app.route('/api/get')
def apiget():
    Result = dbFunc.getIpApi()
    return str(Result)


@app.route('/api/send', methods=['GET','POST'])
def ApiSend():
    if request.method =="POST":
        DataJson =request.json

        dbFunc.postSaveApi(DataJson)

        return "send via post"
    else:
        return "method not is valid"


@app.route('/')
def home():


    Result = dbFunc.getIpApi()

    return render_template("home.html", Result=Result)

if __name__ == '__main__':
    
    port = int(os.environ.get("PORT", 5000))
    app.run(host='0.0.0.0', debug=True, port=port)